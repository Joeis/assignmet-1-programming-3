#pragma once

#ifndef UNITTESTING_H_INCLUDED
#define UNITTESTING_H_INCLUDED


#include "BinarySearchTree.h"
#include "LinkedList.h"

#include <iostream>
#include <string>
#include <assert.h>
#include <vector>
#include <array>
#include "vld.h"

//class BinarySearchTree<;
//class LinkedList;

template<typename TestData>
class BinarySearchTreeUnitTest
{
public:

	BinarySearchTreeUnitTest()
	{
		Test_All();
	}

	~BinarySearchTreeUnitTest()
	{
		delete m_tree;
		m_tree = nullptr;
	}

	bool are_equal(TestData a, TestData b)
	{
		return a == b;
	}

	template <class T>
	bool verify(T expected, T got, const std::string& message)
	{
		if (are_equal(expected, got))
		{
			std::cout << "\nPassed: " << message << std::endl;
			return true;
		}
		std::cout << "Failed! Expected: " << expected << " Got: " << got <<
			" - " << message << std::endl;

		return false;
	}

	template <class T>
	bool verify_missing(T expected, T got, const std::string& message)
	{
		if (are_equal(expected, got))
		{
			std::cout << "Failed! Expected: " << expected << " Got: " << got <<
			" - " << message << std::endl;

			return false;
		}
		
		std::cout << "\nPassed: " << message << std::endl;
		
		return true;

	}

	//TestFuncs

	void Test_All()
	{
		m_tree = new BinarySearchTree<TestData>;

		std::cout << "Inserting (7, 6, 10, 9, 4, 1, 2, 5)\n";
		Test_Insert(7);
		Test_Insert(6);
		Test_Insert(10);
		Test_Insert(9);
		Test_Insert(4);
		Test_Insert(1);
		Test_Insert(2);
		Test_Insert(5);

		Test_Size(8);

		std::cout << "\nSearching for 4\n";
		Test_Search(4);


		Test_Traversal_Pre_Order();
		Test_Traversal_In_Order();
		Test_Traversal_Post_Order();

		std::cout << "\n\nClearing";
		Test_Clear();
	}

	void Test_Insert(TestData value)
	{
		m_tree->Insert(value);

		verify(value, m_tree->Search(value)->value, "Testing Insert");

	}


	void Test_Traversal_Pre_Order()
	{
		m_tree->Traversal_Pre_Order();
	}



	void Test_Traversal_In_Order()
	{
		m_tree->Traversal_In_Order();
	}

	void Test_Traversal_Post_Order()
	{
		m_tree->Traversal_Post_Order();
	}

	void Test_Clear()
	{
		m_tree->Clear();

		if (m_tree->m_rootNodePointer == nullptr)
		{
			std::cout << "\nPassed: Testing clear" << std::endl;
		}
		else
		{
			std::cout << "\nFailed! Testing clear" << std::endl;
		}

	}

	unsigned Test_Size(unsigned expectedSize)
	{

		verify(expectedSize, m_tree->Size(), "Testing size");

		return m_tree->Size();

	}

	void Test_Search(TestData v)
	{
		verify(v, m_tree->Search(v)->value, "Testing size");

	}


private:
	BinarySearchTree<TestData> * m_tree;

};

////////////////////////////////////////////////////////////

template<typename TestData>
class LinkedListUnitTest
{
public:

	LinkedListUnitTest()
	{
		test_all();
	}

	~LinkedListUnitTest()
	{
	}

	template <class Ta>
	bool are_equal(Ta a, Ta b)
	{
		return a == b;
	}

	template <class T>
	bool verify(T expected, T got, const std::string& message)
	{
		if (are_equal(expected, got))
		{
			std::cout << "Passed: " << message << std::endl;
			return true;
		}
		std::cout << "Failed! Expected: " << expected << " Got: " << got <<
			" - " << message << std::endl;
		return false;
	}

	template <class T>
	bool verify_missing(T expected, T got, const std::string& message)
	{
		if (are_equal(expected, got))
		{
			std::cout << "Failed! Expected: " << expected << " Got: " << got <<
				" - " << message << std::endl;

			return false;
		}

		std::cout << "\nPassed: " << message << std::endl;

		return true;

	}


	void test_pop_front()
	{
		TestData value = m_linkedList->firstNode->nextNode->value;

		m_linkedList->pop_front();

		verify(value, m_linkedList->firstNode->value, "Testing pop_front");

	}

	void test_pop_back()
	{
		TestData value = m_linkedList->FindSecondLastNode()->value;

		m_linkedList->pop_back();

		verify(value, m_linkedList->FindLastNode()->value, "Testing pop_back");

	}

	void test_clear()
	{
		m_linkedList->clear();

		if (m_linkedList->firstNode == nullptr)
		{
			std::cout << "Passed: Testing clear" << std::endl;
		}
		else
			std::cout << "Failed! Testing clear" << std::endl;

	}

	void test_push_front(TestData value)
	{
		m_linkedList->push_front(value);

		verify(value, m_linkedList->firstNode->value, "Testing push_front");

	}

	void test_push_back(TestData value)
	{
		m_linkedList->push_back(value);

		verify(value, m_linkedList->FindLastNode()->value, "Testing push_back");

	}

	void test_insert(int index, TestData value)
	{
		m_linkedList->insert(index, value);

		verify(value, m_linkedList->at(index), "Testing insert");

	}


	void test_remove(TestData value)
	{
		ListNode<TestData> * node = m_linkedList->get_node_with_value(value);

		m_linkedList->remove(value);

		verify_missing(value, node->value, "Testing remove");

	}

	void test_all()
	{
		m_linkedList = new LinkedList<TestData>;

		std::cout << "Push back 1, 7, 8\n";
		test_push_back(1);
		test_push_back(7);
		test_push_back(8);
		m_linkedList->printList();

		std::cout << "Push front 10, 3, 6\n";
		test_push_front(10);
		test_push_front(3);
		test_push_front(6);
		m_linkedList->printList();


		std::cout << "Pop back\n";
		test_pop_back();
		m_linkedList->printList();


		std::cout << "Pop front\n";
		test_pop_front();
		m_linkedList->printList();


		std::cout << "Insert 6 at index 3\n";
		test_insert(3, 6);
		m_linkedList->printList();


		std::cout << "Removing 6\n";
		test_remove(6);
		m_linkedList->printList();


		std::cout << "Clering\n";
		test_clear();
		m_linkedList->printList();


		if (m_linkedList->firstNode == nullptr)
			delete m_linkedList;

		std::cout << "End linked list test\n\n\n";
	}


private:
	LinkedList<TestData>* m_linkedList;

};


#endif UNITTESTING_H_INCLUDED