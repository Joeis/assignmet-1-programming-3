#pragma once
#include <iostream>
#include <vector>

template<typename TValue>
struct Node
{
	TValue value;
	Node *rightNode;
	Node *leftNode;
};

template<typename TValue>
class BinarySearchTree
{
public:
	//Public functions

	BinarySearchTree()
	{
		m_rootNodePointer = nullptr;
	}

	~BinarySearchTree()
	{
		Clear();
	}

	void Insert(TValue v)
	{
		if (m_rootNodePointer != nullptr)
		{
			*InsertPrivate(v, m_rootNodePointer) = CreateNewNode(v);
		}
		else
		{
			m_rootNodePointer = CreateNewNode(v);
		}
	}


	void Traversal_Pre_Order()
	{
		if (m_rootNodePointer != nullptr)
		{
			std::cout << "\n\nPrinting tree in pre-order: ";
			Traversal_Pre_Order_Private(m_rootNodePointer);
		}
		else
			std::cout << "\nTree is empty.";
	}

	void Traversal_In_Order()
	{

		if (m_rootNodePointer != nullptr)
		{
			std::cout << "\n\nPrinting tree in order: ";
			Traversal_In_Order_Private(m_rootNodePointer);
		}
		else
			std::cout << "\nTree is empty.";

	}

	void Traversal_Post_Order()
	{
		if (m_rootNodePointer != nullptr)
		{
			std::cout << "\n\nPrinting tree in post order: ";
			Traversal_Post_Order_Private(m_rootNodePointer);
		}
		else
			std::cout << "\nTree is empty.";

	}

	void Clear()
	{
		if (m_rootNodePointer != nullptr)
		{
			ClearPrivate(m_rootNodePointer);
			m_rootNodePointer = nullptr;
		}
	}

	unsigned Size()
	{
		std::cout << "\nSize of the tree: " << SizePrivate(m_rootNodePointer);
		return SizePrivate(m_rootNodePointer);
	}

	Node<TValue>* Search(TValue v)
	{
		if (m_rootNodePointer != nullptr)
		{
			return SearchPrivate(v, m_rootNodePointer);
		}

		return m_rootNodePointer;
	}


private:
	//Private functions

	//Insert functions
	Node<TValue>** InsertPrivate(TValue value, Node<TValue>* node)
	{
		if (node != nullptr)
		{
			if (value >= node->value)
			{
				if (node->rightNode != nullptr)
				{
					return InsertPrivate(value, node->rightNode);
				}
				return &node->rightNode;
			}
			else if (value < node->value)
			{
				if (node->leftNode != nullptr)
				{
					return InsertPrivate(value, node->leftNode);
				}
				return &node->leftNode;
			}
		}
	}
	Node<TValue>* CreateNewNode(TValue v)
	{
		Node<TValue>* newNode = new Node<TValue>;

		newNode->value = v;
		newNode->rightNode = nullptr;
		newNode->leftNode= nullptr;

		return newNode;
	}
	
	//Search
	Node<TValue>* SearchPrivate(TValue v, Node<TValue>* currentNode)
	{
		if (currentNode != nullptr)
		{
			if (v == currentNode->value)
			{

				return currentNode;
			}
			else if (v > currentNode->value)
			{
				return SearchPrivate(v, currentNode->rightNode);
			}
			else if (v < currentNode->value)
			{
				return SearchPrivate(v, currentNode->leftNode);
			}
		}

		std::cout << "\nThe node is not in the tree.";

		return nullptr;
	}

	//Size
	unsigned SizePrivate(Node<TValue>* node)
	{
		unsigned index = 1;

		if (node->leftNode != nullptr)
		{
			index += SizePrivate(node->leftNode);
		}

		if (node->rightNode != nullptr)
		{
			index += SizePrivate(node->rightNode);
		}

		return index;
	}

	//Print functions
	void Traversal_Pre_Order_Private(Node<TValue>* node)
	{
		std::cout << node->value << ", ";
		if (node->leftNode != nullptr)
		{
			Traversal_Pre_Order_Private(node->leftNode);
		}

		if (node->rightNode != nullptr)
		{
			Traversal_Pre_Order_Private(node->rightNode);
		}
	}
	void Traversal_In_Order_Private(Node<TValue>* node)
	{
		if (node->leftNode != nullptr)
		{
			Traversal_In_Order_Private(node->leftNode);
		}
		std::cout << node->value << ", ";

		if (node->rightNode != nullptr)
		{
			Traversal_In_Order_Private(node->rightNode);
		}
	}
	void Traversal_Post_Order_Private(Node<TValue>* node)
	{
		if (node->leftNode != nullptr)
		{
			Traversal_Post_Order_Private(node->leftNode);
		}

		if (node->rightNode != nullptr)
		{
			Traversal_Post_Order_Private(node->rightNode);
		}
		std::cout << node->value << ", ";

	}

	//Clear
	void ClearPrivate(Node<TValue>* node)
	{
		//"Post order" clear method
		if (node->leftNode != nullptr)
		{
			ClearPrivate(node->leftNode);
		}

		if (node->rightNode != nullptr)
		{
			ClearPrivate(node->rightNode);
		}

		node->value = NULL;
		node->rightNode = nullptr; 
		node->leftNode = nullptr;
		delete node;
		node = nullptr;
		
	}

	
	public:
	Node<TValue>* m_rootNodePointer;
};