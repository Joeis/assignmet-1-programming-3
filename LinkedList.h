#pragma once
#include <iostream>

template<typename TValue>
struct ListNode
{
	TValue value;
	ListNode *nextNode;
};

template<typename TValue>
class LinkedList
{
public:
	LinkedList()
	{
	}

	~LinkedList()
	{

	}

	void push_back(TValue value)
	{	

		ListNode<TValue> * nodePointer = CreateNewNode(value);
		
		if (firstNode == nullptr)
		{
			firstNode = nodePointer;
		}
		else
			FindLastNode()->nextNode = nodePointer;

	}

	void push_front(TValue value)
	{
		ListNode<TValue> * newNodePointer = CreateNewNode(value);

		newNodePointer->nextNode = firstNode;
		firstNode = newNodePointer;

	}

	void pop_front()
	{
		ListNode<TValue> * nodePointer = firstNode->nextNode;
		delete firstNode;
		firstNode = nodePointer;
	}

	void pop_back()
	{
		ListNode<TValue> * secondLastNode = FindSecondLastNode();
		delete FindLastNode();
		secondLastNode->nextNode = nullptr;
	}

	void clear()
	{
		while (firstNode != nullptr)
		{
			ListNode<TValue> * nodePointer = firstNode->nextNode;
			delete firstNode;
			firstNode = nodePointer;
		}
	}

	TValue at(int index)
	{
		ListNode<TValue> * nodePointer = firstNode;

		for (int i = 0; i < index; i++)
		{
			if (nodePointer->nextNode != nullptr)
				nodePointer = nodePointer->nextNode;
			else
				break;
		}

		return nodePointer->value;
	}

	int find_value(TValue value)
	{
		int index = 0;
		ListNode<TValue> * nodePointer = firstNode;

		while (nodePointer->value != value)
		{
			if (nodePointer == nullptr)
				break;
			index++;
			nodePointer = nodePointer->nextNode;
		}
		
		return index;
	}
	
	ListNode<TValue> * get_node_with_value(TValue value)
	{
		ListNode<TValue> * nodePointer = firstNode;

		while (nodePointer->value != value)
		{
			if (nodePointer == nullptr)
				break;

			nodePointer = nodePointer->nextNode;
		}

		return nodePointer;
	}

	void insert(int index, TValue value)
	{
		ListNode<TValue> * nodeAtIndex = firstNode;
		ListNode<TValue> * nodeBeforeIndex = firstNode;

		if (index != 0)
		{
			for (int i = 0; i < index; i++)
			{
				nodeAtIndex = nodeAtIndex->nextNode;
			}
			for (int i = 0; i < index - 1; i++)
			{
				nodeBeforeIndex = nodeBeforeIndex->nextNode;
			}
			ListNode<TValue> * newNodePointer = CreateNewNode(value);
		newNodePointer->nextNode = nodeAtIndex;
		nodeBeforeIndex->nextNode = newNodePointer;
		}
		else
		{
			ListNode<TValue> * newNodePointer = CreateNewNode(value);
			newNodePointer->nextNode = firstNode;
			firstNode = newNodePointer;
		}
	}

	void remove(TValue value)
	{
		ListNode<TValue> * nodeWithValue = firstNode;
		ListNode<TValue> * nodeBeforeValue = nullptr;

		while (nodeWithValue->value != value && nodeWithValue->nextNode != nullptr)
		{
			nodeBeforeValue = nodeWithValue;
			nodeWithValue = nodeWithValue->nextNode;
		}

		if (nodeBeforeValue != nullptr)
		{
			nodeBeforeValue->nextNode = nodeWithValue->nextNode;
			delete nodeWithValue;
			nodeWithValue = nullptr;
		}
		else
		{
			firstNode = nodeWithValue->nextNode;
			delete nodeWithValue;
			nodeWithValue = nullptr;
		}
	}

	void printList()
	{
		ListNode<TValue>* currentNode =  firstNode;
		while (currentNode)
		{
			std::cout << currentNode->value << " ,";

			currentNode = currentNode->nextNode;
			
		}
		std::cout << "\n";

	}


	////////////////////////
	//Utilitize
	////////////////////

	//Use this code to get a new pointer: 
	//Node<TValue> * nodePointer = CreateNewNode(value);
	ListNode<TValue>* CreateNewNode(TValue value)
	{
		ListNode<TValue> * nodePointer = new ListNode<TValue>;
		nodePointer->value = value;
		nodePointer->nextNode = nullptr;

		return nodePointer;
	}

	ListNode<TValue>* FindLastNode()
	{

		ListNode<TValue> *tempNode = nullptr;
			
		tempNode = firstNode;

		while (true)
		{
			if (tempNode->nextNode == nullptr)
			{
				return tempNode;
			}
				
			tempNode = tempNode->nextNode;

		}

		return nullptr;
	}

	ListNode<TValue>* FindSecondLastNode()
	{

		ListNode<TValue> *lastNode = firstNode;
		ListNode<TValue> *secondLastNode = nullptr;

		while (true)
		{
			if (lastNode->nextNode == nullptr)
			{
				return secondLastNode;
			}
			secondLastNode = lastNode;
			lastNode = lastNode->nextNode;

		}

		return nullptr;
	}

//private:
	ListNode<TValue> * firstNode = nullptr;

};
